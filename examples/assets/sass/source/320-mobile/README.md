# Tamanhos de tela

Este diretório contém a mesma estrutura da pasta [common][], porém apenas com as regras específicas para o tamanho de tela. Devem seguir exatamente a mesma estrutura mesmo que algum arquivo contenha poucas linhas. Arquivos e diretórios vazios são ignorados.

Ou seja, se tenho a pasta **~/common/modules/navigation/\_main.scss** e tenho especificidades na versão `320-mobile` terei a mesma estrutura, mesmo que exista apenas 1 arquivo no diretório: **~/320-mobile/modules/navigation/\_main.scss**.

Veja as regras para os [tamanhos de tela aqui][responsive].

[↩][top]

[top]: #markdown-header-tamanhos-de-tela
[common]: ../common/
[responsive]: ../#markdown-header-tamanhos-de-tela