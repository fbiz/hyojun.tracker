# Wrappers

Os wrappers funcionam como agrupadores de imports comuns, seja dos módulos que formam um componente, de um grupo de libs e configurações, etc. Estes arquivos contém apenas imports e são utilizados apenas no [output][].

Necessário tomar muito cuidado para evitar imports repetidos, que podem gerar problemas de código duplicado ou sobreposição de variáveis.

Conteúdo da página:

[TOC]

## Saiba quando criar um wrapper

Como a criação de módulos força a fragmentação do código e gera um número elevado de arquivos, é comum que a mesma lista de import é feita nos arquivos de saída ([output][]). Quando isso acontece, um wrapper pode ser criado. Alguns exemplos de wrappers já pré-definidos:

* **wrappers/_core.scss** — import de todos os arquivos de dentro do diretório core:

        @import "../core/config/colors",
                "../core/config/units",
                // ...
                "../core/functions/function-1",
                "../core/functions/function-2",
                // ...
                "../core/mixins/mixin-1",
                // ...
                "../core/extends/extend-1",
                // ...
                "../core/animations/animation-1";

    Em projetos responsivos o config precisa ficar separado do restante do core, para que os imports sejam alternados [^1]. 

* **wrappers/_lib.scss** — import das libs de terceiro, no caso abaixo considerando o bower [^2]:

        @import "bourbon/app/assets/stylesheet/bourbon",
                "gs/sass/gs"
                "normalize-css/normalize";

* **wrappers/_structure.scss** — módulos que formam a estrutura (header, footer, etc.) do projeto:

        @import "../modules/navigation/main",
                "../modules/navigation/footer",
                "../modules/boxes/section-wrapper";

Considerando a página "home", podemos considerar que os imports que teremos abaixo de [output][] seria:

* **output/home.scss** — arquivo que dará saída para css/home.css [^3]:

        @import "../../source/common/wrappers/lib",
                "../../source/common/wrappers/core",
                "../../source/common/wrappers/structure",

                "../../source/common/modules/navigation/highlight-banner",
                "../../source/common/modules/navigation/breadcrumbs",
                "../../source/common/modules/boxes/product-list",
                // ...
                "../../source/common/layouts/home";

[↩][top]

## Wrappers em sites responsivos

Sites responsivos acabam tendo wrappers do "common" e da versão específica. Uma prática para não gerar muitas chamadas de import nos arquivos em [output][] é importar o common dentro da versão específica. Para facilitar o entendimento, este exemplo considera a versão 320-mobile:

* **320-mobile/wrappers/_core.scss** — import de todos os arquivos de dentro do diretório core de common seguidos de 320-mobile:

        @import "../../common/wrappers/lib",
                "../../common/wrappers/core",

                "../core/config/colors",
                "../core/config/units",
                // ...
                "../core/extends/extend-1",
                // ...
                "../core/animations/animation-1";

Em sites responsivos, vale a pena ter um wrapper para cada "página", de forma que os imports de common venham agrupados para todas as versões responsivas (320-mobile, 768-tablet, 960-desktop), evitando muitas linhas duplicadas e dificuldades de manutenção:

* **common/wrappers/home.scss** — import dos módulos que formam a home:

        @import "../../source/common/modules/navigation/highlight-banner",
                "../../source/common/modules/navigation/breadcrumbs",
                "../../source/common/modules/boxes/product-list";

* **320-mobile/wrappers/home.scss** — import dos módulos que formam as especificidades de home na versão mobile precedido pela versão da common:

        @import "../../common/wrappers/structure",
                "../../common/wrappers/home",
                "../modules/navigation/highlight-banner",
                "../modules/navigation/breadcrumbs",
                "../modules/boxes/product-list";

Neste caso, dentro de **output/320-mobile/home.scss** temos o import apenas do wrapper da versão "mobile", já que ele contempla o que vem de common:

    @import "../../../source/320-mobile/wrappers/core",
            "../../../source/320-mobile/wrappers/structure",
            "../../../source/320-mobile/wrappers/home",
            "../../../source/320-mobile/layouts/home";

Não necessariamente existirão os mesmos wrappers de common em mobile, existem situações onde a versão mobile de algum módulo é totalmente diferente da versão desktop. Cada projeto tem suas necessidades. A idéia aqui é entender a ordem de imports para que tudo funcione corretamente: 

1. Core de common;
2. Core de mobile;
3. Módulos de common;
4. Módulos de mobile;
5. Layout de mobile;

Em alguns casos, esta fragmentação pode aumentar. O momento da declaração das variáveis é importante, pois todas as próximas declarações dependem dos valores base. Neste caso podemos separar o core em 2 partes: o config e o restante.

1. Config de common;
2. Config de mobile;
3. Lib local (functions, mixins, extends, animations) de common;
4. Lib local de mobile;
5. Módulos de common;
6. Módulos de mobile;
7. Layout de mobile;

[↩][top]

[^1]: Como os módulos responsivos são chamados após o common, a declaração de variáveis precisa acontecer na sequencia, mas antes de qualquer outro import — extends, animations, funcions, etc. para o caso deles estarem usando alguma variável que pode ter sido sobreposta na versão específica. Veja mais sobre a ordem dos imports em [wrappers em sites responsivos][wrappers-responsive].

[^2]: A Hyojun utiliza o [bower][] como gerenciador de pacotes para o front-end. A chamada do sass recebe o parâmetro `--load-path` apontando a pasta `bower_components`, fazendo com que os diretórios fiquem disponíveis sem a necessidade de caminho relativo / absoluto;

[^3]: Veja mais informações na seção [output][] para entender sobre como funciona a organização da saída dos arquivos;

[wrappers-responsive]: #markdown-header-wrappers-em-sites-responsivos
[top]: #markdown-header-template
[output]: ../../../output/
[bower]: http://bower.io/