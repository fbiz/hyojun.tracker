# Config

Todas as configurações do site. Todas as cores, unidades, famílias tipográficas e declarações de gridsystems do projeto devem estar centralizados nestes arquivos. Caso necessário, novos arquivos de configuração podem ser criados.

Dependendo do tipo de projeto, estes arquivos podem ser extensos e a idéia é ter tudo centralizado e nomeado para facilitar a manutenção para que a alteração ou sobreposição de valores aconteça de forma mais tranquila possível.

As funções usadas nos exemplos estão presentes e explicadas no framework [hyojun.in_visible][].

Conteúdo da página:

[TOC]

## Organização dos diretórios

Este escopo contém a seguinte estrutura:

* **~/sass/source/common/core/config/** - você está aqui;
    * **\_colors.scss** - cores do projeto. [Mais detalhes][colors];
    * **\_grids.scss** - definição de gridsystems utilizadas no projeto. [Mais detalhes][grids];
    * **\_typography.scss** - famílias tipográficas do projeto. [Mais detalhes][typography];
    * **\_units.scss** - unidades de medida (fonte, peso, espaçamento, z-index, etc.) do projeto. [Mais detalhes][units];

[↩][top]

## Colors

Todas as cores do projeto devem estar presentes neste arquivo. A idéia desta organização é ter apenas a quantidade necessária de variação de cores, evitando valores cuja diferenças não são perceptíveis ao olho nu.

**É importante entender que nenhum valor de cor entra direto no CSS de módulos.**

O arquivo de cores está organizado em 5 partes:

* Cor do "tema" - cores estruturais;
* Paleta de cores - variações da estrutura (claros e escuros);
* Exceções - cores que não fazem parte da estrutura;
* Cores "base" (opcional) - agrupamento para reutilização;
* Cores do projeto (opcional) - cores por elementos / aplicação;

Cada seção tem uma função específica que ajuda na organização e manutenção.

[↩][top]

### Cor do tema

Aqui estão as cores estruturais do projeto, normalmente são 3 ou 4 cores.

Estas variáveis seguem o seguinte padrão de nomenclatura: `$theme-[nome]-color` e normalmente recebem o `!default` [^1]. Ex.:

    #!sass
    $theme-main-color: #5e408f !default;
    $theme-neutral-color: #333 !default;
    $theme-highlight-color: #3A87AD !default;

**IMPORTANTE**    
Nunca utilize o próprio nome da cor na variável, utilize um nome abstrato: **não utilize** `$theme-blue-color`, `$theme-orange-color`, etc.

[↩][top]

### Paleta de cores

Com base no "tema", uma paleta de cores é criada com variações claro e/ou escuro. Elas seguem um padrão de nomenclatura: `$group-[nome]-[light ou dark]-[intensidade]`. Ex.:

    #!sass
    $group-main-light10: #654793;
    $group-main-dark10: #432F62;
    $group-main-dark20: #382753;

Em projetos que contém a implementação de cores relativas usando a `hsl-diff` [^2] recebem apenas as chamadas das funções, de forma que quando a cor tema mudar, as cores da paleta acompanharão a mudança:

    #!sass
    $group-main-light10: hsl-diff($theme-main-color, -0.13, 0.81, 6.47); //#654793
    $group-main-dark10: hsl-diff($theme-main-color, -0.28, 1.12, -7.84); //#432F62
    $group-main-dark20: hsl-diff($theme-main-color, -0.63, 2.01, -12.35); //#382753

Para "branco" e "preto" existem 2 nomes especiais `$group-lightest`, `$group-darkest` com os valores correspondentes.

-----

A idéia é que apenas as cores do tema e da paleta sejam usadas no projeto, não existe limite para a quantidade de cores. A idéia desta organização é evitar o uso de valores diferentes que geram resultados muito parecidos, como `#deceff` e `#d6cded`, que não fazem diferença ao olho nu.

Esta organização faz com que a troca de cores do projeto seja um processo **muito rápido**. Ex.: meu projeto tem um tema violeta (#5e408f) e precisa mudar para verde (#46ae30):

    #!sass
    $theme-main-color: #5569b0;

Caso seu projeto use cores dinâmicas, apenas mude o valor de `$theme-main-color`:

    #!sass
    $theme-main-color: #46ae30;

Caso as cores de seu projeto estejam aplicadas manualmente, mude o valor de `$theme-main-color` e de todos os `$goup-main-*`:

    #!sass
    $theme-main-color: #46ae30;
    // ...
    $group-main-light10: #4bc034 !default;
    $group-main-dark10: #388f27 !default;
    $group-main-dark20: #245d19 !default;

**IMPORTANTE**    
No caso de projetos de pequeno porte, estas variáveis podem ser chamadas diretamente nos [módulos][modules]. Para casos de projetos maiores, com suporte a tema ou mudança de paleta de cores, veja os tópicos ["Cores base"][colors-base] e ["Cores do projeto"][colors-project].

[↩][top]

### Exceções

Qualquer cor que não faça parte da paleta e/ou que não faça sentido ser uma "variação" do tema, devem ser colocadas na lista de exceção, como por exemplo cores de selos, labels, logos de terceiros, cores únicas para feedbacks de erro ou sucesso.

**IMPORTANTE**    
No caso de projetos de pequeno porte, estas variáveis podem ser chamadas diretamente nos módulos. Para casos de projetos maiores, com suporte a tema ou mudança de paleta de cores, veja os tópicos ["Cores base"][colors-base] e ["Cores do projeto"][colors-project].

-----

_Os próximos 2 tópicos dizem respeito a um nível maior de granulação, mais comum para projetos grandes, com temas ou necessidade de mudança de paleta, seja por área, seja por data comemorativa._

[↩][top]

### Cores base (opcional)

Em projetos médios ou grandes, com mais elementos e variações, que dão suporte à temas ou mesmo que mudança de paletas para datas comemorativas, etc. cores "base" são altamente recomendadas.

Tanto as cores "base" quando as cores do projeto só são um apontamento de variáveis. O objetivo desta seção é a organização e manutenção de mudanças de cores.

As cores base são utilizadas apenas dentro do próprio arquivo `_colors.scss`.

Um exemplo simples para esclarecer o que seriam as "cores base" é um projeto com cards, tabelas, botões e títulos. Cada um destes elementos possuem uma borda, a princípio com a mesma cor. Nesta seção criaríamos uma variável chamada `$base-border-color` apontando a cor da paleta:

    #!sass
    $base-border-color: $group-main-dark-10 !default;

Podemos chamar esta variável `$base-border-color` diretamente nos [modulos][modules] ou nas cores do projeto, explicado na [próxima seção][colors-project]:

    #!sass
    $button-border-color: $base-border-color !default;
    $table-border-color: $base-border-color !default;
    $card-border-color: $base-border-color !default;
    $title-border-color: $base-border-color !default;

Desta forma, criamos camadas de alteração e manutenção, seguindo a ordem:

1. Se o tema mudar, a paleta que usa aquele tema muda, consequentemente, as cores base e as cores do projeto;
2. Se a cor da paleta muda, cores base e do projeto mudarão;
3. Se a cor base muda, todas as cores do projeto, mudarão;
4. Se alguma cor do projeto não seguir as regras da base, esta pode "pular" a etapa da base e apontar direto pra paleta.

Ou seja, no exemplo acima, se todas as bordas do projeto mudarem de cor não seguindo a variação da paleta, apenas a variável `$base-border-color` precisa ser alterada. Mas se por qualquer motivo apenas as tabelas mudarem de cor, a variável `$table-border-color` é alterada. Toda administração de cores fica centralizada no arquivo `_colors.scss` ao invés de ficar distribuída pelos arquivos dos módulos do SASS.

Projetos pequenos podem não gerar necessidade de cores base, mas é considerado uma boa prática criar estas variáveis para garantir um projeto menos engessado.

[↩][top]

### Cores do projeto (opcional)

Projetos de longo prazo, que podem conter mudanças na paleta ou temas devem declarar variáveis por elemento, que serão utilizadas no restante dos arquivos `.scss`. Estas variáveis apenas apontam para os "temas", "paletas", "exceções" ou "base".

A idéia principal é que cada tipo de elemento do projeto receba sua própria variável e que o redirecionamento seja feito dentro do `_colors.scss`. O nível de granulação e especificidades vai de acordo com o projeto. Para os que demandam mais mudanças, customização, temas, etc. é recomendado que seja mais específico.

Por exemplo, se tenho um card com uma borda com a cor do tema em 2 situações distintas, talvez valha a pena ter 2 variáveis apontando para a mesma cor "base" ao invés de 1, para o caso de customização de temas:

    #!sass
    $base-card-border-color: $group-neutral-light20 !default;
    // ...
    $newsletter-card-border-color: $base-card-border-color !default;
    $profile-card-border-color: $base-card-border-color !default;

Neste caso, se numa customização, apenas a newsletter precisar mudar de cor, a sobreposição de valor será mais simples e apenas no `_colors.scss`. O nível de granulação deve ser definido no começo do projeto, de acordo com a necessidade: é mais comum que grandes portais que dão suporte a temas tenham este tipo de organização.

O [arquivo de cores][colors-file] contém algumas variáveis comuns que podem ajudar a entender a organização.

[↩][top]

### Padrão de nomenclatura de cores

Para garantir que não haja sobreposição acidental de variáveis no projeto, a seguinte nomenclatura deve ser seguida:

* Temas: `$theme-[nome]-color`
    *  ex.: `$theme-main-color`;
* Paletas: `$group-[nome]-["light" ou "dark"]-[intensidade]`
    *  ex.: `$group-main-dark-10`, `$group-main-light-10`, `$group-main-light-20`;
* Base: `$base-[nome]-[tipo][-variacao]`
    *  ex.: `$base-border-color`, `$base-light-border-color`;
* Exceções / cores do projeto: `$[nome]-[tipo][-variacao]`
    *  ex.: `$facebook-bg-color-hover`, `$success-bg-color`, `$success-font-color`;

"Tipos" disponíveis:

* **border-color**: cor de borda, ex.: `$search-input-border-color`;
* **bg-color**: cor de fundo, ex.: `$base-card-bg-color`;
* **box-shadow-color**: sombra de um elemento, ex.: `$base-card-box-shadow-color`;
* **font-color**: cor da fonte, ex.: `$base-card-font-color`;
* **text-shadow-color**: cor da sombra da fonte, ex.: `$highlight-text-shadow-color`;

"Variações" disponíveis:

* **from**: em casos de gradiente, o ponto inicial, ex.: `$search-input-bg-color-from`;
* **to**: em casos de gradiente, o ponto final, ex.: `$search-input-bg-color-to`;
* **hover**: em casos de botões, links, o estado de "hover", ex.: `$base-link-font-color-hover`;
* **active**: em casos de itens ativos, ex.: `$base-link-font-color-active`;

_\* Caso algum "tipo" ou "variação" não esteja disponível nesta lista, [abra uma issue][issues] para que o documento seja complementado._

[↩][top]

## Grids

Todos os projetos utilizam o framework [GS][] [^3]. O arquivo `_grids.scss` contém a declaração de todas as grids necessárias para o projeto.

[Este arquivo][grids-file] já contém algumas chamadas pré-definidas para sites responsivos que podem ajudar a entender a organização.

[↩][top]

## Typography

Todas as variáveis que definem famílias tipográficas e seus fallbacks se encontram no arquivo `_typography.scss`.

[Este arquivo][typography-file] já contém algumas variáveis pré-definidas que podem ajudar a entender a organização.

[↩][top]

## Units

A lógica das unidades do projeto é muito parecida com a de [cores][colors]. As unidades estão presentes [neste arquivo][units-file] e devem conter todas as medidas base do projeto com o objetivo de criar um padrão para unidades, evitando inconsistências nos valores e facilitar a manutenção.

Existem algumas unidades que sempre estarão presentes:

* tamanho e peso das fontes (font-size e font-weight);
* z-index;
* espaçamentos (margin, padding);
* espessura de bordas (border-width);

Algumas outras que podem existir:

* canto arredondado;
* angulos de gradiente;
* line-height para layouts mais tipográficos;
* configuração de componente, como número de colunas, valores que definem espaçamentos internos, etc;

[↩][top]

### Tamanho de fonte

A lógica do tamanho de fonte é muito parecida com a de [cores][colors]. É recomendado que as medidas sejam em [REM][] e/ou [EM][], dependendo da situação. Em caso de suportes a browsers mais antigos, usar `PX` em conjunto com `EM`.

Como as fontes são relativas, existem 2 medidas que contém o tamanho fixo em PX, normalmente recebem o `!default` [^1]:

    #!sass
    $base-font-size: 16px !default;
    $root-font-size: $base-font-size !default;

Onde:

* `$base-font-size`: utilizado apenas no SASS como base de cálculo;
* `$root-font-size`: aplicado no elemento root do projeto;

[↩][top]

### Lista de tamanhos

Lista de fontes do projeto. Assim como o tema, não utilize medidas nos nomes das variáveis, dê nomes abstratos que indiquem a situação. No exemplo abaixo a função `to-rem` [^4] considera `$base-font-size` como base de cálculo:

    #!sass
    $head-font-size: to-rem(60px);
    $title-font-size: to-rem(36px);
    $sub-title-font-size: to-rem(30px);
    $lead-font-size: to-rem(24px);
    $content-font-size: to-rem(16px);
    $discreet-font-size: to-rem(14px);

Assim como na lógica das [cores][colors], também existem as fontes "base" e as fontes do projeto que seguem a mesma regra. O ideal é que todos os tamanhos de fontes estejam no arquivo `_units.scss`.

O nível de granulação pode variar de acordo com a necessidade, tamanho e complexidade do projeto, assim como acontece com [cores][colors].

[↩][top]

### Peso de fonte

Existem alguns pesos de fonte já definidos no CSS que devem ser usados no projeto:

    #!sass
    $light-font-weight: 300;
    $regular-font-weight: 400;
    $medium-font-weight: 500;
    $bold-font-weight: 700;
    $black-font-weight: 900;

Assim, o valor do `font-weight` que é utilizado no projeto fica normalizado. Normalmente não existe a necessidade de nomear de forma abstrata os pesos de fonte.

[↩][top]

### z-index

Para garantir uma lógica na utilização do z-index para o projeto, os valores devem ser definidos com nomenclatura abstrata. Ex.:

    #!sass
    $z-index-behind: 3;
    $z-index-content: 6;
    $z-index-context: 9;
    $z-index-layer: 12;
    $z-index-over: 15;

Note que os elementos estão numerados de 3 em 3. Na aplicação dos módulos, o seguinte formato pode ser utilizado para garantir uma margem e profundidade correta do elemento de acordo com a necessidade ao utilizar `over` ou `under` [^5]. Ex.:

    #!sass
    .elm-active {
        z-index: under($z-index-context); // retorna ($z-index-context - 1)
    }
    .context {
        z-index: $z-index-context;
    }
    .context-active {
        z-index: over($z-index-context);  // retorna ($z-index-context + 1)
    }

[↩][top]

### Espaçamentos

Assim como as cores e tamanhos base, as medidas estruturais do projeto devem estar centralizadas neste local. Após análise do layout e da grid utilizada, os espaçamentos estruturais podem ser definidos para utilização no projeto. Estes estão presentes como medidas "base", no caso abaixo, utilizando o `to-rem` [^4]:

    #!sass
    $base-space-size: to-rem(10px);
    $double-space-size: ($base-space-size * 2);
    $separator-space-size: ($base-space-size * 4);
    $highlight-space-size: to-rem(55px);
    $half-space-size: ($base-space-size / 2);
    $minor-space-size: ($base-space-size / 5);

Seguindo a lógica das cores, os padding, margins ou tamanhos padrões são colocados dentro de `_units.scss`. Ex.:

    #!sass
    $product-photo-size: to-rem(120px);
    $highlight-photo-size: to-rem(240px) to-rem(120px);
    $banner-photo-size: to-rem(1000px) to-rem(500px);
    
    $base-title-margin: $double-space-size 0;
    $base-card-padding: $half-space-size;

Considerando que a margem esperada para um título é de `$double-space-size`, é sabido que este valor será influenciado pelo line-height, dependendo do tipo de fonte. No elemento final, podemos fazer um calculo simples para descontar a diferença e manter o alinhamento perfeito. No exemplo abaixo a função `mt` [^6] e `to-rem` [^4] estão sendo usadas:

    #!sass
    .content-title {
        font-size: $title-font-size;
        margin-top: mt($base-title-margin) - to-rem(4px);
    }

[↩][top]

### Animations

Todas as variáveis base de unidades para animações devem ficar aqui. Estão divididas em `timing-function`, `delay` e `duration`. Normalmente as variações `base`, `slow` e `fast` são criadas no start-up do projeto:

    #!sass
    $base-anim-duration: 0.3s;
    $slow-anim-duration: $base-anim-duration * 2;
    $fast-anim-duration: $base-anim-duration * 0.5;

    // easing (from http://matthewlein.com/ceaser/)
    $base-anim-ease: ease;
    $in-out-anim-ease: cubic-bezier(0.455, 0.030, 0.515, 0.955); // easeInOutQuad
    $bounce-anim-ease: cubic-bezier(0.680, -0.550, 0.265, 1.550); // easeInOutBack

    // delay
    $base-anim-delay: 0.2s;
    $slow-anim-delay: $base-anim-delay * 2;
    $fast-anim-delay: $base-anim-delay * 0.5;

Na implementação do código, usa-se variação da base. Caso a base de cálculo das animações precise ser mais rápida ou mais lenta, apenas este pedaço de código é alterado. As unidades de base não são usadas em casos de exceção onde o tempo da animação é exato e não pode ser alterado.

### Misc

Para medidas que não se enquadram nas situações anteriores, a última seção deve ser utilizada.

[↩][top]

### Padrão de nomenclatura de medidas

Para garantir que não haja sobreposição acidental de variáveis no projeto, a seguinte nomenclatura deve ser seguida:

* `$base-[nome]-[tipo]` para unidades "base", ex.: `$base-title-space-size`;
* `$[peso]-font-size` para tamanhos de fontes, ex.: `$title-font-size`;
* `$[peso]-font-weight` para pesos de fontes, ex.: `$bold-font-weight`;
* `$z-index-[nome]` para tipos de z-index, ex.: `$z-index-content`;

Tipos disponíveis:

* **font-size** - para tamanho de fontes;
* **space-size** - para distâncias abstratas (que podem ser usadas em padding, margin, etc.);
* **padding** - específicos para paddings;
* **margin** - específicos para margin;
* **border-width** - para espessura de bordas;
* **radius-size** - para tamanhos de cantos arredondados;
* **gradient-angle** - para angulos de gradientes;

[↩][top]

[^1]: O `!default` é utilizado para facilitar a sobreposição destes valores, caso necessário. [Mais informações aqui][sass-defaults]. É aconselhável usar esta técnica para cores que são usadas como base de cálculo, como exemplificado na [paleta de cores][colors].

[^2]: Note que neste caso a diferença do HSL sempre será relativa, então se `$theme-main-color` mudar de valor, todas estas variações mudarão proporcionalmente. Para mais informações sobre a função `hsl-diff`, acesse a documentação do [hyojun.in_visible][].

[^3]: GS é um framework para criação de grids responsivas e dá a possibilidade de criar múltiplas grids no projeto, de acordo com a necessidade. Toda a estrutura deve ser criada pegando as medidas destas grids.

[^4]: A função `to-rem` converte pixels para rem. Para mais informações, acesse a documentação do [hyojun.in_visible][].

[^5]: As funções `over` e `under` retornam um z-index acima (+1) ou abaixo (-1) do valor passado. Mais informações na documentação do [hyojun.in_visible][].

[^6]: A função `mt` retorna o valor `margin-top` de uma variável, se baseando na spec para margin. Mais informações na documentação do [hyojun.in_visible][].

[top]: #markdown-header-config
[colors]: #markdown-header-colors
[colors-base]: #markdown-header-cores-base-opcional
[colors-palette]: ##markdown-header-paleta-de-cores
[colors-project]: #markdown-header-cores-do-projeto-opcional
[grids]: #markdown-header-grids
[typography]: #markdown-header-typography
[units]: #markdown-header-units

[colors-file]: _colors.scss
[grids-file]: _grids.scss
[typography-file]: _typography.scss
[units-file]: _units.scss
[modules]: ../../modules/

[sass-defaults]: http://sass-lang.com/documentation/file.SASS_REFERENCE.html#variable_defaults_
[GS]: https://github.com/mcarneiro/gs
[hyojun.in_visible]: https://bitbucket.org/fbiz/hyojun.in-visible
[issues]: https://bitbucket.org/fbiz/hyojun.sass-standards/issues/new
[REM]: http://www.w3.org/TR/css3-values/#rem-unit
[EM]: http://www.w3.org/TR/css3-values/#em-unit
