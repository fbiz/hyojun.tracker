# Mixins

Diretório com todos os mixins do projeto. A idéia é que seja escrito em todos os arquivos presentes na pasta `output`, ou seja, contém apenas declarações e não escrevem nada na saída do CSS. [Veja melhor como funciona a ordem de imports][wrappers].

Conteúdo da página:

[TOC]

## Organização dos diretórios

Este escopo contém a seguinte estrutura:

* **~/sass/source/common/core/extends/** - você está aqui;

[↩][top]

## Entenda quando criar um mixin

Existem algumas diferenças básicas entre `@mixin` e `@extend` — veja aqui [quando criar extends][]. Como o mixin recebe argumentos e escreve seu conteúdo no momento que é chamado, deve ser usado quando é necessário automatizar / calcular a inserção de algumas propriedades e valores. Alguns exemplos de mixins:

* Um pequeno include que é usado com frequencia:

        #!sass
        @mixin reset-list {
          margin: 0;
          padding: 0;
          list-style-type: none;
        }

    Note que o mixin sempre insere o conteúdo no HTML, diferente do extend. Neste caso, se sua lista precisa ter algum margin ou padding, a saída fica duplicada, ex.:

        #!sass
        .photo-gallery {
          @include reset-list;
          margin-top: to-rem(20px);
        }

    A saída será "suja":

        #!css
        .photo-gallery {
          margin: 0;
          padding: 0;
          list-style-type: none;
          margin-top: 1.25rem; // nah :\
        }

    Para evitar esse tipo de saída indesejada, o ideal é inserir um pouco de lógica no mixin:

        #!sass
        @mixin reset-list($margin:0, $padding:0) {
          margin: $margin;
          padding: $padding;
          list-style-type: none;
        }

    Neste exemplo simples, teríamos a implementação com parâmetros:

        #!sass
        .photo-gallery {
          @include reset-list(to-rem(20px) 0 0 0);
        }


* A maioria dos casos acabam indicando algum trabalho repetitivo, como a criação de uma seta usando a técnica com bordas:

        #!sass
        @mixin arrow ($size: 10px, $orientation: 'right', $color: $arrow-base-color){
          width: 0;
          height: 0;
          @if length($size) == 1 {
            border: $size solid transparent;
          } @else {
            border: nth($size, 1) solid transparent;
          }
          @elseif $orientation == 'right' {
            border-left-color: $color;
            border-right-width: 0;
            @if length($size) > 1 {
              border-left-width: nth($size, 1);
              border-top-width: nth($size, 2) * 0.5;
              border-bottom-width: nth($size, 2) * 0.5;
            }
          }
          // ...
        }

      _\* Note que este mixin é apenas para efeito de exemplo._

[↩][top]

[top]: #markdown-header-mixins
[quando criar extends]: ../extends/
[wrappers]: ../../wrappers