# Animations

As eventuais animações em css ficam neste diretório. O objetivo é separar a camada de animação da estrutura de box-modeling para facilitar a manutenção.

## Como criar animações

Cada animação deve ficar num arquivo próprio e seu conteúdo consiste em um [placeholder selectors][] que serão extendidos nos módulos. O nome do extend deve conter o prefixo `anim-` e o nome do arquivo deve seguir o nome do extend sem o prefixo. Ex.:

    #!sass
    // no arquivo button-icon.scss
    %anim-button-next {

        // default
        @include transition-property(background-color);
        @include transition-duration($fast-anim-duration);

        &,
        .ico,
        .anim-wrapper {
            @include transition-timing-function($base-anim-ease);
        }
        .anim-wrapper,
        .ico {
            @include transition-property(transform);
            @include transition-delay($fast-anim-delay);
            @include transition-duration($slow-anim-duration);
        }

        // hover
        &:hover {
            .ico,
            .anim-wrapper {
                @include transition-delay(0);
            }
        }
    }

No exemplo acima, tenho as configurações da animação, usando o bourbon [^1]. A separação do `// hover` com o `// default`, ajuda a identificar melhor o que está acontecendo em cada estado de animação. O mesmo aconteceria para casos como `&.active`, `&.next`, `&.previous`, etc.

Saiba mais sobre as variáveis de unidade `$base-anim-ease`, `$fast-anim-delay`, etc. nas [configurações do projeto][units-animations].

[↩][top]

[top]: #markdown-header-animations
[units-animations]: ../config/#markdown-header-animations
[placeholder selectors]: http://sass-lang.com/documentation/file.SASS_REFERENCE.html#placeholder_selectors_
