# Modules

A maior parte dos arquivos estarão neste diretório. De acordo com a análise do projeto, os módulos devem ser separados e cada um deve estar em um arquivo scss com suas variações. Quanto mais granulado, menos engessado fica o projeto.

Conteúdo da página:

[TOC]

## Organização dos diretórios

Normalmente nenhum arquivo fica direto na pasta `~/source/common/modules`, ao invés disso, existem alguns diretórios que indicam as "categorias" dos módulos. Existem alguns padrões de nomenclatura e agrupamento que são mais comuns:

* **boxes** — para cards, containers, modal, etc;
* **forms** — para elementos e diagramações de formulários;
* **navigation** — para menus, breadcrumbs, paginações, tabs, botões, sliders, etc;
* **tables** — para tabelas;
* **texts** — para títulos, textos padrão, componentes de texto (como preço + descrição), declaração de `@font-face`, etc;
* **lists** — listas de produtos, galerias, listas de posts, etc.

[TODO: completar e explicar melhor esta lista]

## Como criar um módulo

Normalmente quando o projeto é iniciado, uma análise do lay-out deve ser feita para identificar quais partes seguem uma mesma lógica ou estrutura. Com base nisso, o arquivo scss do módulo é criado e salvo no diretório adequado.

Considerando um exemplo de header, `menu-block`, `auth-block`, `search-block` e `logo` seriam módulos.

![Layout de exemplo](http://img-fotki.yandex.ru/get/5008/221798411.0/0_babce_7deef28f_XXL.png)

O scss do módulo não deve considerar o posicionamento dos elementos, ou seja. Um módulo, a princípio deve ser "stand-alone": se eu inserí-lo em qualquer lugar, ele deveria se encaixar sem quebrar o lay-out.

Isso significa que `auth-block` bor exemplo, não pode ter `float` ou `width` fixo pré-definido. O módulo por si só deve ser fluído e se encaixar em qualquer lugar. O posicionamento, tamanhos e espaçamentos necessários são definidos no [layout][].

Note que os elementos de dentro de um módulo podem ter tamanho fixo, float, etc. Usando o exemplo abaixo:

![Módulo de exemplo](http://img-fotki.yandex.ru/get/9109/221798411.0/0_babcc_d935a8ec_XXL.png)

e considerando o arquivo `~/source/common/module/lists/_content-list.scss` para a criação deste módulo. Cada item é considerado como um elemento stand-alone com 3 partes: título, foto e texto.

Como o módulo tem a apresentação em lista, podemos criar um extend "local" — que só será usado neste arquivo para este tipo de caso — do item:

    #!sass
    %content-item {
        margin: $base-space-size 0;
        .content-photo {
            float: left;
            max-width: w($photo);
            margin: 0 $base-space-size $base-space-size 0;
        }
        .content-title {
            font-size: $title-font-size;
            margin: 0 0 $base-space-size;
        }
        .content-description {
            font-size: $content-font-size;
            margin: 0;
        }
    }

Note que para a versão stand-alone, colocamos uma margem padrão, para caso o elemento fosse inserido em qualquer lugar da página, fora da estrutura de lista. Após isso, criamos a classe de saída e sua implementação em lista:

    #!sass
    .content-item {
        @extend %content-item;
    }
    .content-list {
        margin: 0;
        padding: 0;
        list-style-type: none;
        > li {
            @extend %content-item;
            margin-top: $highlight-space-size;
            &:first-child {
                margin-top: 0;
            }
        }
    }

Sendo assim, podemos fazer a implementação do módulo com ou sem lista, sendo que na versão de lista, teremos um espaçamento maior entre os elementos.

A granulação do módulo depende do objetivo do projeto. É recomendável fazer a análise e agrupar tudo que faz sentido agrupar. Por exemplo: se neste caso o título for comum para o projeto, a classe dele poderia ser usada ou mesmo um `@extend` apondando para seu `placeholder`. [Saiba mais sobre como organizar os extends][extends].

[↩][top]

[top]: #markdown-header-template
[layout]: ../layouts/
[extends]: ../core/extends/