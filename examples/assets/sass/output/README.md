# Output

Este diretório contém os arquivos que darão saída para os CSS que serão carregados nas páginas do projeto. A regra básica é: o nome destes arquivos não terão o prefixo `_` e o conteúdo se resume a uma lista de `@import`.

A leitura da seção de [source][], [módulos][] e [wrappers][] é recomendada para entender alguns conceitos deste documento.

Conteúdo da página:

[TOC]

## Organização dos diretórios

Este escopo contém a seguinte estrutura:

* **~/sass/output/** - você está aqui;
    * **320-mobile/** - arquivos que darão saída para versão mobile;
    * **768-tablet/** - arquivos que darão saída para versão tablet;
    * **960-desktop/** - arquivos que darão saída para versão desktop;

## Como funciona

Os arquivos em output contém apenas imports do [core][], libs externas, [módulos][], [layouts][] e [wrappers][].

**IMPORTANTE**    
Apenas output e wrappers podem conter a chamada `@import`. Isso garante maior controle de overrides de mixins, variáveis ou funções e evita código duplicado na saída do CSS.

Veja mais informações de como os imports são organizados em [wrappers][wrappers-how-to].

Dependendo do tipo de projeto podemos ter 1 ou 2 chamadas CSS na por página. Projetos com 1 chamada tem baixa complexidade na estrutura, e acaba ocupando poucos kb.

### Projetos com 1 chamada de CSS

Projetos com 1 chamada normalmente importam a estrutura e o código específico da página no mesmo CSS. O peso em kb pode aumentar um pouco por página, mas temos economia de 1 requisição que gera ganho no tempo de carregamento.

Normalmente temos um CSS por tipo de página, como `product.scss`, `register.scss`, `login.scss`, `home.scss`, etc.

O exemplo abaixo mostra um suposto arquivo `output/960-home/home.scss` sem considerar o uso de [wrappers][], para facilitar o entendimento:

    #!sass
    @import
            ///////////////
            // ESTRUTURA //
            ///////////////
            "bourbon/app/assets/stylesheet/bourbon", // no bower_components
            "gs/sass/gs", // no bower_components
            "normalize-css/normalize", // no bower_components
            // ...
            "../../../source/common/core/config/colors",
            "../../../source/common/core/config/colors",
            "../../../source/common/core/config/units",
            // ...
            "../../../source/common/core/functions/function-1",
            "../../../source/common/core/functions/function-2",
            // ...
            "../../../source/common/core/mixins/mixin-1",
            // ...
            "../../../source/common/core/extends/extend-1",
            // ...
            "../../../source/common/core/animations/animation-1",

            ////////////
            // PÁGINA //
            ////////////
            "../../../source/common/modules/navigation/highlight-banner",
            "../../../source/common/modules/navigation/breadcrumbs",
            "../../../source/common/modules/boxes/product-list",
            "../../../source/960-desktop/modules/navigation/highlight-banner",
            "../../../source/960-desktop/modules/navigation/breadcrumbs",
            "../../../source/960-desktop/modules/boxes/product-list",
            // ...
            "../../../source/960-desktop/layouts/home";

Como a estrutura e listas de módulos normalmente são longas, fica feio e inviável repetir ou dar manutenção destes imports no projeto. O ideal é agrupá-los em [wrappers][]. A saída do arquivo `output/960-desktop/home.scss` ficaria algo assim:

    #!sass
    @import "../../../source/960-desktop/wrappers/core",
            "../../../source/960-desktop/wrappers/structure",
            "../../../source/960-desktop/wrappers/home",
            "../../../source/960-desktop/layouts/home";

No caso acima:

* todas as libs + core estão em `wrappers/core`;
* os módulos estruturais — que aparecem em **todas** as páginas — em `wrappers/structure`;
* todos os módulos usados na seção home estão no `wrappers/home` e
* a diagramação em `layouts/home`.

_Entenda mais sobre [wrappers e a lógica dos imports aqui][wrappers-how-to] pois eles são muito importantes para garantir uma boa organização e entendimento dos arquivos em `output`._

Importante entender que o módulo estrutural contém classes e regras que aparecem em todas as páginas. Elementos "comuns", mas que não são necessariamente globais entram específicos por página ou podem ser agrupados em [wrappers][].

[↩][top]

### Projetos com 2 chamadas de CSS

Projetos com 2 chamadas normalmente contém estrutura mais complexa, imagens ou fontes inseridas como Base64 [^1] no CSS, fazendo com que exista economia de requisições por redução de assets. Os headers `expire` e `max-age` fazem com que essa requisição inicial da estrutura aconteçam apenas na primeira página, custo-benefício para o projeto.

Neste caso, temos sempre um arquivo `structure.scss` e o arquivo específico. O exemplo abaixo mostra a saída do arquivo `output/960-desktop/structure.scss`:

    #!sass
    @import "../../../source/960-desktop/wrappers/core",
            "../../../source/960-desktop/wrappers/structure",
            "../../../source/960-desktop/layouts/structure";

E no caso do arquivo `output/960-desktop/home.scss`:

    #!sass
    @import "../../../source/960-desktop/wrappers/core",
            "../../../source/960-desktop/wrappers/home",
            "../../../source/960-desktop/layouts/home";

_Note que `wrappers/core` é carregado nos 2 arquivos, pois ele apena declara variáveis, mixins e funções. Entenda mais sobre [wrappers e a lógica dos imports aqui][wrappers-how-to] pois eles são muito importantes para garantir uma boa organização e entendimento dos arquivos em outputs._

[↩][top]

## Sites responsivos

Entenda melhor a organização dos [imports em sites responsivos][wrappers-reponsive].

[↩][top]

[^1]: A inserção de asset como base64 deve ser feita com muito cuidado. É necessário estudar o que será inserido e o seu impacto no peso do CSS. Por exemplo: uma fonte que foi exportada apenas com os glyphs necessários e fica com 10, 15kb e é utilizada em todas as páginas do projeto pode ser inserida como base64. Já se esta mesma fonte é usada com todas as variações de peso e itálico, outra solução deve ser usada. Normalmente fontes de ícones, imagens ou SVGs leves e estruturais podem ser inseridas como base64.

[top]: #markdown-header-output

[wrappers-how-to]: ../source/wrappers/#markdown-header-saiba-quando-criar-um-wrapper
[wrappers-reponsive]: #markdown-header-wrappers-em-sites-responsivos
[source]: ../source/
[core]: ../source/common/core
[módulos]:  ../source/common/modules
[layouts]:  ../source/common/layouts
[wrappers]:  ../source/common/wrappers