var example = 0;
var _gaq = {
	push: function(p_arr) {
		console.info("p_arr: ", p_arr);
		var arr = (p_arr || []).concat();
		$('.response').eq(example).text('_gaq.push([\'' + arr[0] + '\',\'' + (!!!arr.shift() || arr.join('\',\'')) + '\']);');
		console.log($('.response'));
	}
}
hyojun.tracker.track().config({
	'trackingObject': '_gaq',
	'method': 'push',
	'timeout': 1000,
	'map': {
		'area': {
			'test1': {
				'params': [['_trackEvent', 'este é o teste 1', 'param 2', 'param 3 {dina}']]
			},
			'test2': {
				'params': [['_trackPageview', 'teste 2']]
			},
			'form': {
				'params': [['_trackEvent', 'form', 'sucesso']]
			}
		}
	}
});
$(function() {
	$('#test1').click(function(e) {
		example = 0;
		hyojun.tracker.track('area.test1');
		e.preventDefault();
	});
	$('#test2').click(function(e) {
		example = 1;
		hyojun.tracker.track('area.test1', { 'dina': 'valor-dinamico' });
		e.preventDefault();
	});
	$('#test3').click(function(e) {
		example = 2;
		hyojun.tracker.track('area.test2').url(this.href, this.target);
		e.preventDefault();
	});
	$('#test4').click(function(e) {
		example = 3;
		hyojun.tracker.track('area.test2').url(this.href, this.target);
		e.preventDefault();
	});
	$('#test5').click(function(e) {
		example = 4;
		hyojun.tracker.track('area.test3').url(this.href, this.target);
		e.preventDefault();
	});
	$('#test6').click(function(e) {
		example = 5;
		hyojun.tracker.track('area.test1').js(function() { alert('script executado!'); });
		e.preventDefault();
	});
	$('#test7').submit(function(e) {
		example = 6;
		hyojun.tracker.track('area.form').submit(this);
		e.preventDefault();
	});
});