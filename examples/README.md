# Hyojun.guideline example page bootstrap

Esse projeto foi criado para a criação de páginas de exemplo usando a estrutura de criação de páginas e lay-outs da guideline.

## Instalação

Para inserí-lo no projeto, adicione-o utilizando a técnica de subtree:

```
git remote add -f hyojun.guideline git@bitbucket.org:fbiz/hyojun.guideline.git
git merge -s ours --no-commit hyojun.guideline/example-bootstrap
git read-tree --prefix=examples -u hyojun.guideline/example-bootstrap
```

Desta forma fica mais fácil a atualização dos elementos estruturais nos projetos filhos que o usarem fazendo apenas:

```
git pull -s subtree --no-commit hyojun.guideline example-bootstrap
```