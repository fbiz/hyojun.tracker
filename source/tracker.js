/*jshint white:false, browser:true, maxerr:50, onecase:true */
/*!
 * F.biz - Marcelo Miranda Carneiro - mcarneiro@fbiz.com.br, mcarneiro@gmail.com
 * Veja a documentacao em https://desenvsvn.fbiz.com.br:3525/svn/tecnologia_tracker/trunk/jsdoc/index.html (login e senha necessario)
 * Veja pagina de exemplo em https://desenvsvn.fbiz.com.br:3525/svn/tecnologia_tracker/trunk/www/index.html (login e senha necessario)
 * 
 * Histórico de versões
 * 0.1.2
 *   Problema chamada de classe assíncrona;
 *   map labels agora são case-insensitive;
 * 0.1.1
 *   Problema comparação da "delay";
 * 0.1
 *   Primeiro release público
 */
if (!window.hyojun) { var hyojun = {tracker: {}}; }
if (!hyojun.tracker) {
	hyojun.tracker = {};
}

(function (scope) {
	"use strict";
	
	var is = function (p_value, p_is) {
			return p_value && p_value.constructor === p_is;
		},
		objectFromString = function (p_value, p_root) {
			var namespace = (p_value || '').split('.'),
				curr = p_root || window;
			for(var i=0, len=namespace.length; i < len; i++) {
				if (!(curr = curr[namespace[i]])) { return null; }
			}
			return curr;
		},
		lowerLabels = function (p_data) {
			var obj = {};
			for(var n in p_data) {
				if (p_data.hasOwnProperty(n)) {
					obj[n.toLowerCase()] = is(p_data[n], Object) ? lowerLabels(p_data[n]) : p_data[n];
				}
			}
			return obj;
		},
		instance,

		Tracker = function (p_map, p_trackingObject, p_method) {
			this.map = p_map;
			this.trackingObject = p_trackingObject;
			this.method = p_method;
			this.timeout = 250;
			this._currentTimeout = null;
			this._queue = [];
		};
	
	Tracker.prototype =
	{
		_parseParam: function (p_param, p_extra) {
			var ret;
			switch(true) {
				case is(p_param, String):
				case is(p_param, Number):
					var value = (((p_param+'').match(/\{(.*?)\}/) || []).pop() || '').toLowerCase();
					if (value) {
						switch(true) {
							case (is(p_extra[value], String)):
							case (is(p_extra[value], Number)):
							case (!p_extra[value]):
								ret = p_param.replace(/\{.*?\}/, p_extra[value] || '');
								break;
							default:
								ret = p_extra[value];
								break;
						}
					}
					break;
				case is(p_param, Array):
					ret = this._parseWildcards(p_param, p_extra);
					break;
				case is(p_param, Object):
					for(var n in p_param) {
						if (p_param.hasOwnProperty(n)) {
							ret = this._parseParam(p_param[n], p_extra);
						}
					}
					break;
			}
			return ret || p_param;
		},
		_parseWildcards: function (p_params, p_extra) {
			// TODO: criar possibilidade de passar mais de 1 wildcard numa string
			// TODO: criar possibilidade de "tipar" para fazer verificação e soltar mensagens de erros para callbacks ou itens obrigatórios, por exemplo
			// TODO: parametros com object
			var ret = [];
			for(var i=0, len=p_params.length; i < len; i++) {
				if (p_params[i]) {
					ret.push(this._parseParam(p_params[i], p_extra));
				}
			}
			return ret;
		},
		_extraShortcuts: function (p_extra) {
			p_extra = is(p_extra, Function) ? {'callback': p_extra} : p_extra;
			return p_extra;
		},
		_runQueue: function () {
			var callback, params;
			for(var i=0, len=this._queue.length; i < len; i++) {
				if (is(this._queue[i], Array)) {
					callback = this._queue[i][0];
					params = this._queue[i][1] || [];
					if (is(callback, Function)) {
						callback.apply(params[0], params.slice(1));
					}
				}
			}
			this._queue.length = 0;
		},

		config: function (p_config) {
			if (!p_config) { return; }
			var props = ['timeout', 'map', 'trackingObject', 'method'];
			for(var i=0, len=props.length; i < len; i++) {
				if (p_config[props[i]]) {
					this[props[i]] = p_config[props[i]];
				}
			}
			return this;
		},

		getTrackingObject: function() {
			return is(this.trackingObject, String) ? window[ this.trackingObject ] : this.trackingObject;
		},
		
		track: function (p_map, p_extra) {

			p_extra = this._extraShortcuts(p_extra);
			
			var data = objectFromString(p_map, this.map) || {},
				params = (data.params || []).concat(),
				extra = p_extra ? lowerLabels(p_extra) : {};
				data.method = data.method || this.method;
			
			if (!data.params) {
				if (is(extra.callback, Function)) {
					extra.callback();
				}
				return this;
			}

			params = this._parseWildcards(params, extra);
			
			if (!this.getTrackingObject()) {
				throw new Error("[Tracker#track] É necessário configurar o trackingObject para que a marcação seja feita.");
			}
		
			if (is(this.getTrackingObject()[data.method], Function)) {
			
				this.getTrackingObject()[data.method].apply(this.getTrackingObject(), params);
			}
			return this;
		},
		

		has: function (p_map) {
			return !!objectFromString(p_map, this.map);
		},
		
		delay: function (p_value) {
			clearTimeout(this._currentTimeout);
			var _this = this;
			this._currentTimeout = setTimeout(function () {
				_this._currentTimeout = null;
				_this._runQueue();
			}, p_value != null ? p_value : this.timeout);
			return this;
		},
		
		url: function (p_url, p_target) {
			if (!p_target || p_target === '_self') {
				this.delay().js(function () {
					document.location.href = p_url;
				});
			}else{
				this.js(function () {
					window.open(p_url, p_target);
				});
			}
			return this;
		},
		
		submit: function (p_form) {
			this.delay().js(function () {
				if (p_form) {
					p_form.submit();
				}
			});
			return this;
		},

		js: function (p_callback, p_params, p_scope) {
			this._queue.push([p_callback, (p_scope || [null]).concat(p_params)]);
			if (!this._currentTimeout) {
				this._runQueue();
			}
			return this;
		}
	};
	Tracker.VERSION = "0.1.2";
	
	instance = new Tracker();

	scope.track = function (p_map, p_extra) {
		return arguments.length === 0 ? instance : instance.track(p_map, p_extra);
	};
	scope.Tracker = Tracker;

})(
	hyojun.tracker
);